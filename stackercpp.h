#include <opencv2/opencv.hpp>
#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/video/tracking.hpp>
#include <opencv2/videostab/motion_stabilizing.hpp>


using namespace std;
using namespace cv;

#ifndef POSTPROCESSD__STACKERCPP_H
#define POSTPROCESSD__STACKERCPP_H

class Stacker {
public:
        Stacker(bool verbose);

        void
        add_frame(unsigned char *data, int width, int height);

        char *
        get_result();

        char *
        postprocess(unsigned char *data, int width, int height);

        int
        get_width();

        int
        get_height();

private:
        bool verbose;
        int layers;
        cv::Mat reference;
        cv::Mat stacked;
        clock_t stopwatch;
        int export_width;
        int export_height;
        float trimratio;

        Mat
        postprocess_mat(Mat input);

        void
        stopwatch_start();

        void
        stopwatch_mark(const char *name);
};

#endif //POSTPROCESSD__STACKERCPP_H
